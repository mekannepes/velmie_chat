const express = require('express')
const ApiController = require('../controller/api')

const router = express.Router()


router.get("/users",ApiController.Users)

router.get("/groups", ApiController.Groups)

module.exports = router