const users = require('../index')
const groups = require('../index')
const jsonWebToken = require('jsonwebtoken')


// GET => '/api/users'
module.exports.Users = (req, res) => {
    
    const authHeader = req.get("Authorization")

    const token = authHeader.replace("Bearer ", "")

    let userID = jsonWebToken.decode(token)

    let user = []

    users.users.forEach(u => {
        if(u.id != userID){
            let c = {}
            c.user_id = u.id,
            c.name = u.name,
            c. surname = u.surname

            user.push(c)
        }
    });

    return res.status(200).json(user)
}

// GET => '/api/groups'
module.exports.Groups = (req, res) => {
    
    let group = groups.groups.map((g,i)  => {       
        return {
            id:i,
            name:g.name
        }
    })

    return res.status(200).json(group)
}

