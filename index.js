const express = require('express')
const http = require('http')
const jsonWebToken = require('jsonwebtoken')
const socketIO = require('socket.io')
const authRouter = require('./routes/auth')
const authMiddleware = require("./middleware/authMiddleware")
const apiRouter = require('./routes/api')

const jwtSecret = require('./configs/app')

const app = express()

app.use(express.json())

//users express
const users = []
//clients socket
const clients = new Map()
//groups socket
const groups = []

app.use((req,res,next) => {
    res.header('Access-Control-Allow-Origin','*');
    res.header('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept, Authorization');

    if (req.method === 'OPTIONS') {
        res.header('Access-Control-Allow-Methods','POST, PUT, PATCH, DELETE, GET');

        return res.status(200).json({})
    }
    next()
})

app.use("/",authRouter)
app.use("/api",authMiddleware,apiRouter)
app.get("/",(req,res) => {
    res.send(200).json
})
const server = http.createServer(app)
const io = socketIO(server)

//middleware for socket 
io.use((socket, next) => {

    if(socket.handshake.query.token){
        const token = socket.handshake.query.token.replace("Bearer ","")

        try {
            jsonWebToken.verify(token,jwtSecret.jwtSecret)
        } catch (error) {          
            next(new Error('Invalid token'))
        }
        next()            
    }
    next(new Error('Token not provided'))
});

io.on("connection", (socket) => {

    const token = socket.handshake.query.token.replace("Bearer ","")
    
    let userID = jsonWebToken.decode(token)

    console.log(userID + " connected. Socket id: ", socket.id)

    clients.set(userID,socket.id)

    socket.on("messages", (data,cb) => {
        
        if(data.type === "group"){
    
            let id = data.group_id
            let message = data.message
    
            let nameGroup = groups[id]
    
            if(nameGroup){
                cb()
                socket.broadcast.to(nameGroup).emit("messages",{type:"group",user_id:userID,group_id:id,message:message})
    
            }else{
                
            }
    
        }else if(data.type === "user"){
    
            let friendId = data.user_id
            let message = data.message
            
            cb()
    
            let friendConnId = clients.get(friendId)
            io.to(friendConnId).emit('messages',{type:"user",user_id:userID,message:message})
    
        }
    })

    socket.on("createGroup",(data,cb) => {
        let name = data.name
    
        groups.push(name)
        socket.join(name)
    
        cb({message:"created", groupId:groups.length-1})
    
    })

    socket.on("joinToGroup",(data, cb) => {
        let id = data.id
    
        socket.join(groups[id])
    
        cb({message:"joined", groupId:id})
    
    })

    socket.on("disconnect", () => {
        console.log("user disconnected")
        clients.delete(userID)
    })
})


server.listen(3001,() => {
    console.log("Server started on port:",3001)
})


module.exports.users = users
module.exports.groups = groups
