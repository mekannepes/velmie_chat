const users = require('../index')
const jwtSecret = require('../configs/app')
const jsonWebToken = require('jsonwebtoken')


// POST => '/login'
module.exports.Login = (req, res) => { 

    const {email, password} = req.body

    const idx = users.users.findIndex(i => i.email === email)
    if(idx != -1){
        let user = users.users[idx]

        if(user.email == email  &&  user.password == password){

            const token = jsonWebToken.sign(idx,jwtSecret.jwtSecret)

            res.status(200).json({
                msg:"OK",
                "token":token
            })
            return 

        }else{
            res.sendStatus(401)
            return
        }
    }else{
        res.sendStatus(401)
    }    
}

// POST => '/register'
module.exports.Register = (req, res) => { 
    const {email} = req.body

    let user = {}

    user.name = req.body.name
    user.surname = req.body.surname
    user.email = req.body.email
    user.password = req.body.password
    user.id = users.users.length

    const idx = users.users.findIndex(i => i.email === email)
    if(idx == -1){

        users.users.push(user)

        const token = jsonWebToken.sign(users.users.length-1,jwtSecret.jwtSecret)

        res.status(200).json({
            msg:"OK",
            "token":token
        })
        return 
    }

    res.sendStatus(401)

}
