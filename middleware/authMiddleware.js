const jwt = require('jsonwebtoken')
const jwtSecret = require('../configs/app')

module.exports = (req, res, next) => {

    const authHeader = req.get("Authorization")
    if(!authHeader){
        return res.status(401).json({
            "error":"error",
            "message":"Token not provided"
        })
    }

    const token = authHeader.replace('Bearer ','')

    try {
        jwt.verify(token,jwtSecret.jwtSecret)
    } catch (error) {
        if(error instanceof jwt.JsonWebTokenError){
            return res.status(401).json({
                "error":"error",
                "message":"Invalid token"
            })
        }
        return res.status(401).json({
            "error":"error",
            "message":"Invalid token"
        })

    }

    next()
}
